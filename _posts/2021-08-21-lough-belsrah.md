---
layout: post
title: Lough Belsrah
tags: [Sliabh Aughty, Historical Sites, Local History, Lakes]
comments: true
---
Lough Belsrah is a small lake in the Slieve Aughty Mountains that lies in a valley between two hills with Sonnagh Windfarm on one side. The lake is split between 3 townlands, Sonnagh Old, Kilnagappagh, and X. 

PHOTOS

There are several similar valleys between the lake and Cloughan Trig Pillar, the ground is very spongy in these spots!

# Folklore

> About two miles west of the Sonnagh School is situated a lonely moorland lake known as Lough Vale Shragh. Approaching it from Loughrea it appears as a gap between two hills. There are many old stories told about this weird uncanny lake. Years ago it is said that it changed from one place to another and some of the old people would actually point out where it once lay.
About eighty years ago there lived in Roxboro five brothers named Persse and the lake and surrounding district belonged to them.
At this time trout was very plentiful in the lake and the Parish Priest of KIlchreest delighted in a day's fishing there. One day these brothers came to the lake and found the priest fishing and being a bigoted, Protestant family they ordered him off. An angry interchange took place and it is said that the priest cursed the lake as never since is fish being found in it. Some years ago a local young man put live trout in it but still no fishing has been successful in its deep waters.
Some time after the priest cursed the lake the Persses brought a boat up to the lake to enjoy a day's rowing; but no sooner had they reached the middle of the lake than a great big fish appeared and capsized the boat. Only for one of them Frank Persse was able to swim they would all have been drowned.
One night as two men were passing by the lake they saw two horses swimming in it and their manes almost covered the lake. The banshee is still to be heard crying round it.
The people of this village believe in fairies and say that they people the shore of this lake. They never like to linger near it after nightfall.

> Tá Loch Béal Sraith suidhte suas sa sléibhte eidir dhá chnoc, timcheall leat bhealaigh eidir an sgoil seo agus Baile-Locha-Ríach, treasna na sléibhe. Tá an loch fada agus caol agus tá clocha mór thart air. Tá bláthanna móra, boga agus fiadháine ag fás ar taobh amháin de agus nuair a líonann an loch ritheann an t-uisge amach ar an dtaobh eile dhe.
Deireann cuid de na daoine go bhfacadar ainmhidhe mór sa loch úair amháin nuair a bhíodar ag snámh. Chualaidh mé nach ndeachaidh duine ar bith ag snámh ann ó shoin mar go raibh faitchíos orra. Chualaidh mé freisin gur chuir duine éigin Cóiste Bodhar suas go dtí an áit sin ón caisleán atá í gClochan. Tá sé ráidhte go raibh an Cóiste ag imtheacht ar an dtalamh mar píle olna. Tá sé ráidhte go raibh an loch sin taobh shuas go teach Fionnagáin ar dtús agus go raibh sé líonta lé bric, acht chuir sagart mallacht ar agus chúaidh an loch suas go dtí an áit in-a bhfuil sé anois. Oshoin níl breac ar bith sa loch, acht tá go leór eascon ann, agus cinn móra freisin.
Mo mhuinntir a d' innis é sin dhom

> Lough Belsrah is situated up on the mountains between two hills, (WHAT?) between this school and Loughrea, across the mountain. The lake is long and narrow and there are big rocks around it. There are big flowers, soft? and wild growing on one side and when the lake fills the water runs out on the other side of it.
Some people say that they saw large animals in the lake one time when they were swimming. I heard that no one swims there at all since then because they're afraid of it. I heard too that someone put the Cóiste Bodhar up to that place from the castle in Cloughan. It is said that the Cóiste was going on the ground like a [Píle Olna]. It is said that that lake was up beside Finnegan's house at the start and that it was full of trout, but a priest put a curse on it and the lake went down to the place it is now. Since then there's no trout in the lake at all, but there are lots of eels, and big ones too.
My family told me this.

## Finnegan's House
http://www.census.nationalarchives.ie/pages/1911/Galway/Kilchreest/Gortnagleav/464396/
http://www.census.nationalarchives.ie/pages/1901/Galway/Kilchreest/Gortnaglean/1387401/
http://griffiths.askaboutireland.ie/gv4/z/zoomifyDynamicViewer.php?file=116128&path=./pix/116/&rs=3&showpage=1&mysession=2773688979000&width=&height=
http://www.askaboutireland.ie/griffith-valuation/index.xml?action=doNameSearch&PlaceID=548955&county=Galway&barony=Loughrea&parish=Killinan&townland=%3Cb%3EGortnagleav%3C/b%3E
http://griffiths.askaboutireland.ie/gv4/single_layer/i8.php?lat=&longt=&dum=0&sheet=114,115&mysession=2773688979000&info=&place=&county=GALWAY&placename=GORTNAGLEAV&parish=KILLINAN&country=Ireland&union=&barony=LOUGHREA

> Bhí sagart ag déanamh stáisiúin i gcomharsanacht an locha agus chonnaic sé bodach Protastúnaigh ag iasgaireacht ann, agus bhí cnap mór d'iasg gabhtha aige agus diarr sé ceann dá dhinnéar. Nidh nach bhfuair agus mhallachtaigh sé an loch agus gach a rabh ann, agus ó soin níl smidín d'iasg le fághail ann.

# Map

# Links
https://www.duchas.ie/en/cbes/4583349/4581896/4606125
https://www.duchas.ie/en/cbes/4583346/4581603/4592708
https://www.duchas.ie/en/cbes/4583348/4581848/4606048?ChapterID=4583348