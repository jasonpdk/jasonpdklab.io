---
layout: post
title: Ruins in Toorleitra
tags: [Sliabh Aughty, Historical Sites, Local History, Ruins]
comments: true
---

[Toorleitra](https://www.townlands.ie/galway/leitrim/ballynakill/marblehill/toorleitra/) is a townland in the Electoral Division of Loughatorick, the parish of Ballinakill, the Barony of Leitrim, and the County Galway. There are many ruins in the townland.

*This post is a Work in Progress.*

![Toorleitra Griffith Valuation Map]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/toorleitra_griffith_valuation_map.png' | relative_url }})

## [Josie Molloy's House (3B)](#josie-molloys-house)
![Thornton's House Toorleitra 25" Map]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_25inch_map.png' | relative_url }})

![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_1.JPG' | relative_url }})
![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_2.JPG' | relative_url }})
![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_3.JPG' | relative_url }})
![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_4.JPG' | relative_url }})
![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_5.JPG' | relative_url }})
![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_6.JPG' | relative_url }})
![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_7.JPG' | relative_url }})
![Josie Molloy's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/josie_molloys_house/josie_molloys_house_toorleitra_8.JPG' | relative_url }})

### Census
[1911](http://www.census.nationalarchives.ie/pages/1911/Galway/Loughatorick/Toorleitra/465764/?fbclid=IwAR22PPDdHDL7IQGu_7vktTli5E_JZF5PbyyLWapyebqkqzfRWsBGDyjhmyg)

## [Thornton's/McMahon's House (12)](#thorntons-house)
![Thorntons's House Toorleitra 25" Map]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_25inch_map.png' | relative_url }})

![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_1.JPG' | relative_url }})
![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_2.JPG' | relative_url }})
![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_3.JPG' | relative_url }})
![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_4.JPG' | relative_url }})
![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_5.JPG' | relative_url }})
![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_6.JPG' | relative_url }})
![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_7.JPG' | relative_url }})
![Thornton's House Toorleitra]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/thorntons_house_toorleitra_8.JPG' | relative_url }})

### Griffith Valuation
![Thornton's Toorleitra Griffith Valuation]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/ruins/toorleitra/thorntons_house/records/griffith_valuation/thorntons_toorleitra_griffith_valuation.png' | relative_url }})

## Census
[1901](http://www.census.nationalarchives.ie/pages/1901/Galway/Loughatorick/Toorleitra/1388807/)  
[1911](http://www.census.nationalarchives.ie/pages/1911/Galway/Loughatorick/Toorleitra/465763/)

# Map
<iframe src="https://www.google.com/maps/d/embed?mid=1HGwrQ_nE5YN1iq0qck-fxRB95GDj7PJ9&ehbc=2E312F" width="640" height="480"></iframe>

# Links
[O'Donovan's Field Books](http://places.galwaylibrary.ie/place/46239)  
[Logainm](https://www.logainm.ie/ga/20133)  
[Griffith Valuation](http://www.askaboutireland.ie/griffith-valuation/index.xml?action=doNameSearch&PlaceID=552210&county=Galway&barony=Leitrim&parish=Ballynakill&townland=%3Cb%3EToorleitra%3C/b%3E)  
[1901 Census](http://www.census.nationalarchives.ie/pages/1901/Galway/Loughatorick/Toorleitra/)  
[1911 Census](http://www.census.nationalarchives.ie/pages/1911/Galway/Loughatorick/Toorleitra/)  
[Tithe Applotment Books](http://titheapplotmentbooks.nationalarchives.ie/search/tab/results.jsp?county=Galway&parish=Ballynakill&townland=Toorleitra&search=Search)  