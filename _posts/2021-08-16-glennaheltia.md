---
layout: post
title: "Glennaheltia: A Pre-Famine Village in Coppanagh"
tags: [Sliabh Aughty, Historical Sites, Local History, Ruins]
comments: true
---

Glennaheltia (or Gleann na gCoillte) is a pre famine village in the townland of Coppanagh in Killeenadeema ED. A story from the School's Collection from Aille School tells us that there were 12 families living there and that they all died during the famine.

![Glennaheltia School's Collection Story]({{ 'img/ruins/coppanagh/glennaheltia/CBES_0059_0426.jpg' | relative_url }})

> There is a mountain in Cuppinagh named Gleann na gCoillte. It is a quarter of a mile from Burns. There were twelve families living in it at one time and they all died during the famine. The walls of the old ruins are still standing. There is no one at all living there now. It is all a stretch of mountain except the place of a little garden outside each old ruin. It is owned by a man named Murphy. This is the way that Murphy got that land. The gentlemen of Clanrickard estate used to be hunting for grouse during the season. One day one of the gentlemen came into Murphhys father. He had stayed in the mountain until a heavy fog came and he lost his way in the mountain. Night came on him and he saw a light and went towards it. The old family of the Murphys that was in it at that time. They gave him lodging. When Gleann na gCoillte was going to be divided that gentleman went to Clanrickard and told him to give it to Murphy for his kindness to himself. So that was how Murphy got Gleann na gCoillte.
*“[The Schools’ Collection, Volume 0059, Page 0426](https://www.duchas.ie/en/cbes/4583350/4581989/4606193)” by Dúchas © National Folklore Collection, UCD is licensed under CC BY-NC 4.0.*

Life would have been tough for the people living there, [a description of Coppanagh from O'Donovan's Field Name Books](http://places.webworld.org/place/24896) gives us an idea of what it was like.

> This is an extensive townland nearly all waste boggy mountain. There are a few spots cultivated which produce very bad crops. The inhabitants are so poor as to want the ordinary comforts of life, rugs, shoes, bedclothes, etc. and their cabins of the most wretched description in which they live with their stock, which constitute their only possessions.

The houses are situated in a valley not far from the entrance to Derrybrien Windfarm. The area has been clear felled in the last few years which makes walking around it difficult. It has been planted again recently so the houses will unfortunately be swallowed up by forestry again.
![Glennaheltia View]({{ 'img/ruins/coppanagh/glennaheltia/views/IMG_6795.JPG' | relative_url }})
![Glennaheltia View]({{ 'img/ruins/coppanagh/glennaheltia/views/IMG_6929.JPG' | relative_url }})

There are 10 houses marked in the area on the 6" Ordnance Survey map, with 4 more in nearby areas called Boleydavy, Pollawaghta, and Sruhaunboy.

![Glennaheltia Map]({{ 'img/ruins/coppanagh/glennaheltia/gleann_na_gcoillte_map_numbers_web.png' | relative_url }})

*This post is a work in progress, there are pictures of 6 of the houses here. I'll add the rest when I get to them.*

## House 2
![Glennaheltia House 2]({{ 'img/ruins/coppanagh/glennaheltia/house_2/IMG_6775.JPG' | relative_url }})
![Glennaheltia House 2]({{ 'img/ruins/coppanagh/glennaheltia/house_2/IMG_6753.JPG' | relative_url }})
![Glennaheltia House 2]({{ 'img/ruins/coppanagh/glennaheltia/house_2/IMG_6746.JPG' | relative_url }})
![Glennaheltia House 2]({{ 'img/ruins/coppanagh/glennaheltia/house_2/IMG_6738.JPG' | relative_url }})
![Glennaheltia House 2]({{ 'img/ruins/coppanagh/glennaheltia/house_2/IMG_6898.JPG' | relative_url }})

## House 3
![Glennaheltia House 3]({{ 'img/ruins/coppanagh/glennaheltia/house_3/IMG_6783.JPG' | relative_url }})
![Glennaheltia House 3]({{ 'img/ruins/coppanagh/glennaheltia/house_3/IMG_6785.JPG' | relative_url }})
![Glennaheltia House 3]({{ 'img/ruins/coppanagh/glennaheltia/house_3/IMG_6804.JPG' | relative_url }})
![Glennaheltia House 3]({{ 'img/ruins/coppanagh/glennaheltia/house_3/IMG_6809.JPG' | relative_url }})

## House 4
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6833.JPG' | relative_url }})
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6839.JPG' | relative_url }})
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6845.JPG' | relative_url }})
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6850.JPG' | relative_url }})
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6857.JPG' | relative_url }})
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6861.JPG' | relative_url }})
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6879.JPG' | relative_url }})
![Glennaheltia House 4]({{ 'img/ruins/coppanagh/glennaheltia/house_4/IMG_6889.JPG' | relative_url }})

## House 5
![Glennaheltia House 5]({{ 'img/ruins/coppanagh/glennaheltia/house_5/IMG_6900.JPG' | relative_url }})
![Glennaheltia House 5]({{ 'img/ruins/coppanagh/glennaheltia/house_5/IMG_6907.JPG' | relative_url }})

## House 6
![Glennaheltia House 6]({{ 'img/ruins/coppanagh/glennaheltia/house_6/IMG_6969.JPG' | relative_url }})
![Glennaheltia House 6]({{ 'img/ruins/coppanagh/glennaheltia/house_6/IMG_6970.JPG' | relative_url }})
![Glennaheltia House 6]({{ 'img/ruins/coppanagh/glennaheltia/house_6/IMG_6966.JPG' | relative_url }})
![Glennaheltia House 6]({{ 'img/ruins/coppanagh/glennaheltia/house_6/IMG_6981.JPG' | relative_url }})
![Glennaheltia House 6]({{ 'img/ruins/coppanagh/glennaheltia/house_6/IMG_7003.JPG' | relative_url }})
![Glennaheltia House 6]({{ 'img/ruins/coppanagh/glennaheltia/house_6/IMG_7006.JPG' | relative_url }})

## House 7
This house isn't much more than a pile of stones now, the forestry isn't doing them any good.
![Glennaheltia House 7]({{ 'img/ruins/coppanagh/glennaheltia/house_7/IMG_7016.JPG' | relative_url }})

# Map
<iframe src="https://www.google.com/maps/d/embed?mid=1OwAW5yXUG1Blhe50Oa2QGaPmqpV9k6A_" width="640" height="480"></iframe>

# Links
[Glennaheltia - O'Donovan's Field Name Books](http://places.webworld.org/place/25189)