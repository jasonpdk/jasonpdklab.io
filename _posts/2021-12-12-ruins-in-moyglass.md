---
layout: post
title: Ruins in Moyglass
tags: [Sliabh Aughty, Historical Sites, Local History, Ruins]
comments: true
---

[Moyglass](https://www.townlands.ie/galway/leitrim/ballynakill/marblehill/moyglass/) is a townland in the Electoral Division of Marblehill, the parish of Ballinakill, the Barony of Leitrim, and the County Galway. There are many ruins in the townland.

*This post is a Work in Progress.*

![Moyglass Griffith Valuation Map]({{ 'img/ruins/moyglass/moyglass_griffith_valuation_map.png' | relative_url }})

## [Hayes' House (12)](#hayess-house)
![Moran's House Laggoo 25" Map]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_25inch_map.png' | relative_url }})

![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_1.JPG' | relative_url }})
![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_2.JPG' | relative_url }})
![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_3.JPG' | relative_url }})
![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_4.JPG' | relative_url }})
![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_5.JPG' | relative_url }})
![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_6.JPG' | relative_url }})
![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_7.JPG' | relative_url }})
![Hayes' House Moyglass]({{ 'img/ruins/moyglass/hayes_house/hayes_house_moyglass_8.JPG' | relative_url }})

### Griffith Valuation
![Hayes' Moyglass Griffith Valuation]({{ 'img/ruins/moyglass/hayes_house/records/griffith_valuation/hayes_moyglass_griffith_valuation.png' | relative_url }})

# Map
<iframe src="https://www.google.com/maps/d/embed?mid=1bR75ani1RryjVKXgw0gCQNonHHbb3iEd&ehbc=2E312F" width="640" height="480"></iframe>

# Links
[O'Donovan's Field Books](http://places.webworld.org/place/46090)  
[Logainm](https://www.logainm.ie/ga/20199)  
[Griffith Valuation](http://www.askaboutireland.ie/griffith-valuation/index.xml?action=doNameSearch&PlaceID=552103&county=Galway&barony=Leitrim&parish=Ballynakill&townland=%3Cb%3EMoyglass%3C/b%3E)  
[1901 Census](http://www.census.nationalarchives.ie/pages/1901/Galway/Marble_Hill/Moyglass/)  
[1911 Census](http://www.census.nationalarchives.ie/pages/1911/Galway/Marblehill/Moyglass/)  
[Tithe Applotment Books](http://titheapplotmentbooks.nationalarchives.ie/search/tab/results.jsp?county=Galway&parish=Ballynakill&townland=Moyglass&search=Search)  