---
layout: post
title: Cullenagh Standing Stone
tags: [Sliabh Aughty, Historical Sites, Local History]
comments: true
---

There is a standing stone in [Cullenagh (Ballinakill, Galway)](https://www.townlands.ie/galway/leitrim/ballynakill/marblehill/cullenagh/) marked as "Stone" on the 6" OS map. It's on the border of three townlands, Cullenagh, Reynabrone, and Drum.

![Cullenagh Standing Stone Map]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/cullenagh_standing_stone/cullenagh_standing_stone_map.png' | relative_url }})

The stone is 1.5m high and is in the junction of two walls. The [Historic Environment Viewer](https://maps.archaeology.ie/HistoricEnvironment/) refers to it as a possible standing stone.

![Cullenagh Standing Stone Historic Environment Viewer]({{ 'img/cullenagh_standing_stone/cullenagh_standing_stone_heviewer.png' | relative_url }})

![Cullenagh Standing Stone]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/cullenagh_standing_stone/IMG_3933.JPG' | relative_url }})
![Cullenagh Standing Stone]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/cullenagh_standing_stone/IMG_3893.JPG' | relative_url }})
![Cullenagh Standing Stone]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/cullenagh_standing_stone/IMG_3922.JPG' | relative_url }})
![Cullenagh Standing Stone]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/cullenagh_standing_stone/IMG_3911.JPG' | relative_url }})
![Cullenagh Standing Stone]({{ 'https://keane-content.ams3.digitaloceanspaces.com/img/cullenagh_standing_stone/IMG_3914.JPG' | relative_url }})

# Map
<iframe width="100%" width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=53.102298,-8.525609&amp;key=AIzaSyBVNC6dbEIPRjV2os7cRJfSaEh7WLjx9ZQ&maptype=satellite"></iframe>

# Links
[Cullenagh - O'Donovan's Field Name Books](http://places.galwaylibrary.ie/place/45705)  
[Cullenagh - Logainm](https://www.logainm.ie/ga/20185)  
[Cullenagh - Griffith Valuation](http://www.askaboutireland.ie/griffith-valuation/index.xml?action=doNameSearch&PlaceID=551865&county=Galway&barony=Leitrim&parish=Ballynakill&townland=%3Cb%3ECullenagh%3C/b%3E)  